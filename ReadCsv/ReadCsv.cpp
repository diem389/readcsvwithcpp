// ReadCsv.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

using namespace std; 

// trim from start (in place)
static inline void ltrim(std::string &s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
		return !std::isspace(ch);
	}));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
		return !std::isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
	ltrim(s);
	rtrim(s);
	s.shrink_to_fit();
}


int main()
{
	fstream input_file; 
	input_file.open("test.csv");
	if (input_file.is_open() == false)
	{
		cout << "File open failed!\n"; 
		return -1;  
	}

	
	int i = 0; 
	string buf; 
	vector<string> extracted; 
	while (std::getline(input_file, buf))
	{		
		
		cout << "[" << i << "] Buf lenghth: "<< buf.length() << endl;
		cout << buf << endl;
		
		std::stringstream ss1(buf);
		while (std::getline(ss1, buf, '"'))
		{
			stringstream ss2(buf);
			while (std::getline(ss2, buf, ','))
			{
				trim(buf);
			
				if(!buf.empty())
					extracted.push_back(buf);
			}

			if (getline(ss1, buf, '"'))
			{
				trim(buf);
				if (!buf.empty())
					extracted.push_back(buf);
			}
		}
	}


	for (int i = 0; i < extracted.size(); i++)
	{
		cout << "[" << i << "] = " << extracted[i] << endl;
	}
}

